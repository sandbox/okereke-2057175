<div class="<?php if (isset($classes)) print $classes; ?>" id="<?php print $block_html_id; ?>"<?php print $attributes; ?>>
<div class="start-box start-post">
<div class="start-box-body start-post-body">
<article class="start-post-inner start-article">
<?php print render($title_prefix); ?>
<?php if ($block->subject): ?>
<h2 class="start-postheader"><?php print $block->subject ?></h2>
<?php endif;?>
<?php print render($title_suffix); ?>
<div class="start-postcontent">
<div class="start-article content">
<?php print $content; ?>
</div>
</div>
<div class="cleared"></div>
</article>
<div class="cleared"></div>
</div>
</div>
</div>
