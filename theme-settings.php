<?php
// Form override fo theme settings
function startup_form_system_theme_settings_alter(&$form, $form_state) {

  // Create a set / group of settings to be listed separately from the core theme settings
  $form['options_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Theme Specific Settings'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE
  );
 
  $form['options_settings']['clear_registry'] = array(
    '#type' => 'checkbox',
    '#title' =>  t('Rebuild theme registry on every page.'),
    '#description'   =>t('During theme development, it can be very useful to continuously rebuild the theme registry. WARNING: this is a huge performance penalty and must be turned off on production websites.'),
    '#default_value' => theme_get_setting('clear_registry'),
  );
 
}